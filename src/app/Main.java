import java.io.IOException;
import java.io.File;
import java.io.FileWriter;
import java.io.BufferedWriter;
import java.io.PrintWriter;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class Main
{
    public static PrintWriter writer;
    
    public static void main(String[] args)
    {
        try {
            writer = new PrintWriter(
                new BufferedWriter(
                    new FileWriter("slc.log", true)));
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }

        Executors.newScheduledThreadPool(1).scheduleAtFixedRate(new Runnable() {
                public void run() {
                    if (null != Main.writer) {
                        Main.writer.println("prova");
                        if (Main.writer.checkError()) {
                            throw new RuntimeException("Error writing file.");
                        }
                    }
                }
            }, 0, 1, TimeUnit.SECONDS);
    }
}
