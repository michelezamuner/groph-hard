package util;

import java.util.EventObject;

public class WatcherEvent extends EventObject
{
	public WatcherEvent(Object source)
	{
		super(source);
	}
}