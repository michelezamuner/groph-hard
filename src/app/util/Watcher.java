package util;

import java.io.File;

import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Arrays;

public class Watcher
{
	protected WatcherFactory factory;
	protected Map<File, List<WatcherEvents>> events;
	protected Map<WatcherDirectory, List<File>> files;

	public Watcher(WatcherFactory factory)
	{
		this.factory = factory;
		events = new HashMap<File, List<WatcherEvents>>();
		files = new HashMap<WatcherDirectory, List<File>>();
	}

	public boolean isWatching(File file)
	{
		return events.keySet().contains(file);
	}

	public void watchFile(File file)
	{
		watchFile(file, WatcherEvents.MODIFY);
	}

	public void watchFile(File file, WatcherEvents ... events)
		throws IllegalArgumentException
	{
		if (this.events.keySet().contains(file)) {
			ArrayList<WatcherEvents> currentEvents = (ArrayList<WatcherEvents>)this.events.get(file);
			for (WatcherEvents event : events) {
				if (!currentEvents.contains(event))
					currentEvents.add(event);
			}
		} else {
			this.events.put(file, new ArrayList<WatcherEvents>(Arrays.asList(events)));
			WatcherDirectory directory = factory.createParentDirectory(file);
			if (files.keySet().contains(directory))
				files.get(directory).add(file);
			else
				files.put(directory, new ArrayList<File>(Arrays.asList(new File[]{ file })));
		}
	}
	
	public void stopWatchingFile(File file)
		throws IllegalArgumentException
	{
		assertFileWatched(file);
		this.events.remove(file);
	}

	public void stopWatchingFile(File file, WatcherEvents ... events)
		throws IllegalArgumentException
	{
		assertFileWatched(file);
		
		ArrayList<WatcherEvents> currentEvents = (ArrayList<WatcherEvents>)this.events.get(file);
		for (WatcherEvents event : events) {
			if (!currentEvents.contains(event))
				throw new IllegalArgumentException("Event " + event + " is not being watched on file " + file + ".");
		}
		
		for (WatcherEvents event : events) currentEvents.remove(event);
		
		if (0 == currentEvents.size()) this.events.remove(file);
	}

	public List<WatcherEvents> getEvents(File file)
		throws IllegalArgumentException
	{
		assertFileWatched(file);
		return events.get(file);
	}

	public WatcherEvents[] getDirectoryEvents(WatcherDirectory directory)
		throws IllegalArgumentException
	{
		assertDirectoryWatched(directory);
		List<WatcherEvents> events = new ArrayList<WatcherEvents>();

		for (File file : files.get(directory)) {
			for (WatcherEvents event : getEvents(file)) {
				if (!events.contains(event))
					events.add(event);
			}
		}

		return events.toArray(new WatcherEvents[events.size()]);
	}
	
	public List<File> getWatchedFiles(WatcherDirectory directory)
		throws IllegalArgumentException
	{
		assertDirectoryWatched(directory);
		return files.get(directory);
	}

	public void addListener(File file, WatcherListener listener)
		throws IllegalArgumentException
	{

	}

	public void startWatching()
	{
		// Pool size may depend on the number of processors
		// for example Runtime.getRuntime().availableProcessors()
		//Executor executor = new FixedThreadPool(2);

		for (WatcherDirectory directory : files.keySet()) {
			directory.setEvents(getDirectoryEvents(directory));
			/*executor.execute(new Runnable() {
				public void run() {
					while (true) {
						for (Map<File, List<WatcherEvents>> events : directory.watch()) {

						}
					}
				}
			});*/
		}
	}
	
	protected void assertFileWatched(File file)
	{
		if (!events.keySet().contains(file))
			throw new IllegalArgumentException("File " + file + " is not being watched.");
	}

	protected void assertDirectoryWatched(WatcherDirectory directory)
	{
		if (!files.keySet().contains(directory))
			throw new IllegalArgumentException("Directory " + directory + " is not being watched");
	}
}