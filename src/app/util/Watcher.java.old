package util;

import java.io.IOException;
import java.io.File;

import java.nio.file.WatchEvent;

import java.util.ArrayList;
import java.util.Map;

public interface Watcher
{
	public void setDirectory(File dir)
		throws IllegalArgumentException, UnsupportedOperationException;

	public File getDirectory()
		throws UnsupportedOperationException;

    public void addFile(String fileName)
        throws UnsupportedOperationException, IllegalArgumentException;

	public void addFile(String fileName, WatchEvent.Kind<?>... events)
    	throws UnsupportedOperationException, IllegalArgumentException;

    public Map<String, ArrayList<WatchEvent.Kind<?>>> getWatchedFiles();

    public boolean isWatching(String fileName, WatchEvent.Kind<?>... events)
        throws IllegalArgumentException;

    public void removeFile(String fileName)
        throws IllegalArgumentException;

    public void removeFile(String fileName, WatchEvent.Kind<?>... events)
    	throws IllegalArgumentException;

    public void addListener(String filename, WatcherListener listener)
        throws IllegalArgumentException;

    public void startWatching()
        throws UnsupportedOperationException, IOException;
}
