package util;

public enum WatcherEvents {
	CREATE,
	DELETE,
	MODIFY
}