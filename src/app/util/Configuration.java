package util;

import java.io.IOException;
import java.io.File;

public interface Configuration
{
    public String get(String key);
    
    public void load(File file)
        throws IOException;

    public void watch();
}
