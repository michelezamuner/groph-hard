package util;

import java.util.Properties;
import java.io.IOException;
import java.io.File;
import java.io.FileReader;

public class SimpleConfiguration
    implements Configuration
{
    private Properties properties;
    private File file;

    public SimpleConfiguration(File file)
        throws IOException
    {
        properties = new Properties();
        load(file);
    }

    public String get(String key)
    {
        return properties.getProperty(key);
    }

    public void load(File file)
        throws IOException
    {
        this.file = file;
        try (FileReader reader = new FileReader(file)) {
            properties.load(reader);
        } catch (IOException e) {
            throw e;
        }
    }

    public void watch()
    {
        
    }
}
