package util;

import java.io.File;

import java.util.Map;
import java.util.HashMap;
import java.util.List;

public class WatcherDirectory
{
	protected File dir;

	public WatcherDirectory(File dir)
	{
		this.dir = dir;
	}

	public boolean exists()
	{
		return true;
	}

	public void setEvents(WatcherEvents[] events)
	{

	}

	public Map<File, List<WatcherEvents>> watch()
	{
		return new HashMap<File, List<WatcherEvents>>();
	}
}