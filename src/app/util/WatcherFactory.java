package util;

import java.io.File;
import java.io.IOException;

public class WatcherFactory
{
	public Watcher createWatcher()
	{
		return new Watcher(this);
	}

	public WatcherDirectory createDirectory(File dir)
	{
		return new WatcherDirectory(dir);
	}
	
	public WatcherDirectory createParentDirectory(File file)
		throws IllegalArgumentException
	{
		try {
			file = file.getCanonicalFile();
		} catch (IOException e) {
			throw new IllegalArgumentException(e);
		}
		
		return new WatcherDirectory(file.getParentFile());
	}
}