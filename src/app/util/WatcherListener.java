package util;

import java.util.EventListener;

public interface WatcherListener extends EventListener
{
	public void onModify(WatcherEvent event);
	public void onCreate(WatcherEvent event);
	public void onDelete(WatcherEvent event);
}