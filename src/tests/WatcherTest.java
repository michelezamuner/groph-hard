import org.junit.Test;
import org.junit.Before;
import org.junit.After;

import static org.junit.Assert.*;

import org.junit.Ignore;
import org.junit.Rule;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.*;

import java.io.IOException;
import java.io.File;

import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;
import java.util.HashMap;

import util.Watcher;
import util.WatcherFactory;
import util.WatcherEvent;
import util.WatcherEvents;
import util.WatcherListener;
import util.WatcherDirectory;

@RunWith(MockitoJUnitRunner.class)
public class WatcherTest
{
	@Rule
	public ExpectedException exception = ExpectedException.none();
	
	protected WatcherFactory factory;
	protected Watcher watcher;
	
	@Before
	public void setUp()
		throws IOException
	{
		factory = mock(WatcherFactory.class);
		when(factory.createParentDirectory(any(File.class))).thenReturn(mock(WatcherDirectory.class));
		watcher = new Watcher(factory);
	}

	@After
	public void tearDown()
		throws IOException
	{
		watcher = null;
		factory = null;
	}
	
	/*@Test
	public void throwExceptionIfParentDoesNotExist()
	{
		try {
			watcher.watchFile(new File(""));
			exception.expect(IllegalArgumentException.class);
		} catch (IllegalArgumentException e) {}
	}*/
	
	public void linkFileToDirectory()
	{
		File file1 = new File("file1");
		File file2 = new File("file2");
		
		WatcherDirectory directory = mock(WatcherDirectory.class);
		when(factory.createParentDirectory(any(File.class))).thenReturn(directory);
		watcher = new Watcher(factory);
		
		watcher.watchFile(file1);
		assertEquals(1, watcher.getWatchedFiles(directory).size());
		assertTrue(watcher.getWatchedFiles(directory).contains(file1));
		
		watcher.watchFile(file2);
		assertEquals(2, watcher.getWatchedFiles(directory).size());
		assertTrue(watcher.getWatchedFiles(directory).contains(file1));
		assertTrue(watcher.getWatchedFiles(directory).contains(file2));
	}
	
	@Test
	public void remembersFileEventsAssociation()
	{
		File file = new File("");
		
		watcher.watchFile(file);
		assertTrue(watcher.isWatching(file));
		
		List<WatcherEvents> events = watcher.getEvents(file);
		assertEquals(1, events.size());
		assertTrue(events.contains(WatcherEvents.MODIFY));
	}
	
	@Test
	public void throwExceptionIfGettingEventsOfNotWatchedFile()
	{
		File file = new File("");
		
		assertFalse(watcher.isWatching(file));
		try {
			watcher.getEvents(file);
			exception.expect(IllegalArgumentException.class);
		} catch (IllegalArgumentException e) {}
	}
	
	@Test
	public void throwExceptionIfStopWatchingNonWatchedFile()
	{
		try {
			watcher.stopWatchingFile(new File(""));
			exception.expect(IllegalArgumentException.class);
		} catch (IllegalArgumentException e) {}
	}
	
	@Test
	public void throwExceptionIfStoppedWatchingNonWatchedEvent()
	{
		File file = new File("");
		
		watcher.watchFile(file, WatcherEvents.CREATE);
		try {
			watcher.stopWatchingFile(file, WatcherEvents.DELETE);
			exception.expect(IllegalArgumentException.class);
		} catch (IllegalArgumentException e) {}
	}
	
	@Test
	public void checkEventsJuggling()
	{
		File file = new File("");
		
		watcher.watchFile(file, WatcherEvents.MODIFY, WatcherEvents.DELETE);
		assertEquals(2, watcher.getEvents(file).size());
		assertTrue(watcher.getEvents(file).contains(WatcherEvents.MODIFY));
		assertTrue(watcher.getEvents(file).contains(WatcherEvents.DELETE));
		
		watcher.stopWatchingFile(file, WatcherEvents.MODIFY);
		assertEquals(1, watcher.getEvents(file).size());
		assertTrue(watcher.getEvents(file).contains(WatcherEvents.DELETE));
		
		watcher.stopWatchingFile(file, WatcherEvents.DELETE);
		assertFalse(watcher.isWatching(file));
		
		watcher.watchFile(file, WatcherEvents.CREATE, WatcherEvents.DELETE);
		assertTrue(watcher.isWatching(file));
		assertEquals(2, watcher.getEvents(file).size());
		assertTrue(watcher.getEvents(file).contains(WatcherEvents.CREATE));
		assertTrue(watcher.getEvents(file).contains(WatcherEvents.DELETE));
		
		watcher.stopWatchingFile(file);
		assertFalse(watcher.isWatching(file));
	}

	@Test
	public void knowsAllEventsRelatedToADirectory()
	{
		File file1 = new File("file1");
		File file2 = new File("file2");
		WatcherDirectory directory = mock(WatcherDirectory.class);
		when(factory.createParentDirectory(any(File.class))).thenReturn(directory);
		Watcher watcher = new Watcher(factory);

		watcher.watchFile(file1);
		watcher.watchFile(file2, WatcherEvents.DELETE);

		List<WatcherEvents> events = new ArrayList<WatcherEvents>(Arrays.asList(watcher.getDirectoryEvents(directory)));
		assertEquals(2, events.size());
		assertTrue(events.contains(WatcherEvents.MODIFY));
		assertTrue(events.contains(WatcherEvents.DELETE));
	}

	@Test
	public void directoryIsGivenTheRightEventsToWatch()
	{
		File file1 = new File("file1");
		File file2 = new File("file2");
		WatcherDirectory directory1 = mock(WatcherDirectory.class);
		WatcherDirectory directory2 = mock(WatcherDirectory.class);
		when(factory.createParentDirectory(file1)).thenReturn(directory1);
		when(factory.createParentDirectory(file2)).thenReturn(directory2);
		Watcher watcher = new Watcher(factory);

		watcher.watchFile(file1);
		watcher.watchFile(file1, WatcherEvents.DELETE);
		watcher.watchFile(file2, WatcherEvents.CREATE, WatcherEvents.MODIFY);
		watcher.startWatching();

		verify(directory1).setEvents(new WatcherEvents[] { WatcherEvents.MODIFY, WatcherEvents.DELETE });
		verify(directory2).setEvents(new WatcherEvents[] { WatcherEvents.CREATE, WatcherEvents.MODIFY });
	}
	
	/*@Ignore
	class TestListener implements WatcherListener
	{
		public boolean isModify;
		public boolean isCreate;
		public boolean isDelete;
		@Override
		public void onModify(WatcherEvent event) { isModify = true; }
		@Override
		public void onCreate(WatcherEvent event) { isCreate = true; }
		@Override
		public void onDelete(WatcherEvent event) { isDelete = true; }
	}

	@Test
	public void checkListeners()
	{
		File file1 = new File("file1");
		File file2 = new File("file2");
		WatcherDirectory directory = mock(WatcherDirectory.class);
		when(factory.createParentDirectory(any(File.class))).thenReturn(directory);

		Map<File, List<WatcherEvents>> events = new HashMap<File, List<WatcherEvents>>();
		events.put(file1, new ArrayList<WatcherEvents>(Arrays.asList(new WatcherEvents[] { WatcherEvents.MODIFY })));
		events.put(file2, new ArrayList<WatcherEvents>(Arrays.asList(new WatcherEvents[] { WatcherEvents.CREATE })));
		when(directory.watch()).thenReturn(events);

		Watcher watcher = new Watcher(factory);
		watcher.watchFile(file1, WatcherEvents.MODIFY);
		watcher.watchFile(file2, WatcherEvents.CREATE);

		TestListener listener = new TestListener();
		watcher.addListener(file1, listener);
		watcher.addListener(file2, listener);

		watcher.startWatching();
		assertTrue(listener.isModify);
		assertTrue(listener.isCreate);
	}*/
}