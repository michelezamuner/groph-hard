import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Rule;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;

import org.mockito.runners.MockitoJUnitRunner;
import static org.mockito.Mockito.*;

import java.io.IOException;
import java.io.File;

import util.WatcherFile;

@RunWith(MockitoJUnitRunner.class)
public class WatcherFileTest
{
	@Rule
	public ExpectedException exception = ExpectedException.none();
}