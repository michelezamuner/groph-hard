import org.junit.Assert;
import org.junit.Test;
import org.junit.Rule;
import org.junit.Before;
import org.junit.After;
import org.junit.Ignore;
import org.junit.rules.ExpectedException;

import util.Watcher;
import util.SimpleWatcher;
import util.WatcherListener;
import util.WatcherEvent;

import java.io.IOException;
import java.io.File;

import java.nio.file.WatchEvent;
import java.nio.file.StandardWatchEventKinds;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.Map;
import java.util.Calendar;

public class WatcherTest
{
	@Rule
	public ExpectedException exception = ExpectedException.none();

	protected Watcher unsetWatcher;
	protected Watcher watcher;
	protected File testFs;
	protected File testFile;
	protected File testFile1;

	public WatcherTest()
		throws IOException
	{
		super();
		testFs = new File(System.getProperty("tests.fs")).getCanonicalFile();
		testFile = new File(testFs.getPath() + "/testFile");
		testFile1 = new File(testFs.getPath() + "/testFile1");
	}

	@Before
	public void setUp()
		throws IOException
	{
		unsetWatcher = new SimpleWatcher();
		watcher = new SimpleWatcher();
		watcher.setDirectory(testFs);
		testFile.createNewFile();
		testFile1.createNewFile();
	}

	@After
	public void tearDown()
		throws IOException
	{
		unsetWatcher = null;
		watcher = null;
		testFile.delete();
		testFile1.delete();
	}

	@Test(expected=IllegalArgumentException.class)
	public void shouldThrowExceptionWatchingNonExistingDirectory()
	{
		unsetWatcher.setDirectory(new File("/non/existing/directory"));
	}

	@Test(expected=UnsupportedOperationException.class)
	public void shouldThrowExceptionTryingToOverwriteDirectory()
	{
		watcher.setDirectory(testFs);
	}

	@Test
	public void shouldThrowExceptionGettingNotSetDirectory()
	{
		try {
			unsetWatcher.getDirectory();
			exception.expect(UnsupportedOperationException.class);
		} catch (UnsupportedOperationException e) {}

		try {
			try {
				unsetWatcher.setDirectory(new File(testFs.getPath() + "/nonExistentDirectory"));
			} catch (IllegalArgumentException e) {}

			unsetWatcher.getDirectory();
			exception.expect(UnsupportedOperationException.class);
		} catch (IllegalArgumentException | UnsupportedOperationException e) {}

	}

	@Test(expected=UnsupportedOperationException.class)
	public void shouldThrowExceptionAddingFileToNotSetDirectory()
		throws UnsupportedOperationException, IllegalArgumentException
	{
		unsetWatcher.addFile("nonExistentFile");
	}

	@Test(expected=IllegalArgumentException.class)
	public void shouldGetExceptionAddingNonExistentFile()
		throws UnsupportedOperationException, IllegalArgumentException, IOException
	{
		watcher.addFile("nonExistentFile");
	}

	@Test
	public void shouldRememberFileEventAssociation()
	{
		String fileName = testFile.getName();
		watcher.addFile(fileName);

		ArrayList<WatchEvent.Kind<?>> events = watcher.getWatchedFiles().get(fileName);
		Assert.assertEquals(1, events.size());
		Assert.assertEquals(StandardWatchEventKinds.ENTRY_MODIFY, events.get(0));

		watcher.addFile(fileName, StandardWatchEventKinds.ENTRY_CREATE, StandardWatchEventKinds.ENTRY_DELETE);
		events = watcher.getWatchedFiles().get(fileName);
		Assert.assertEquals(3, events.size());
		Assert.assertTrue(events.contains(StandardWatchEventKinds.ENTRY_CREATE));
		Assert.assertTrue(events.contains(StandardWatchEventKinds.ENTRY_DELETE));

		String fileName1 = testFile1.getName();
		watcher.addFile(fileName1, StandardWatchEventKinds.ENTRY_DELETE);
		events = watcher.getWatchedFiles().get(fileName1);
		Assert.assertEquals(1, events.size());
		Assert.assertEquals(StandardWatchEventKinds.ENTRY_DELETE, events.get(0));
	}

	@Test
	public void shouldCorrectlyCheckWatchedFiles()
	{
		try {
			boolean isWatching = watcher.isWatching("nonExistentFile");
			exception.expect(IllegalArgumentException.class);
		} catch (IllegalArgumentException e) {}

		String fileName = testFile.getName();
		watcher.addFile(fileName, StandardWatchEventKinds.ENTRY_MODIFY, StandardWatchEventKinds.ENTRY_DELETE);
		Assert.assertTrue(watcher.isWatching(fileName, StandardWatchEventKinds.ENTRY_MODIFY, StandardWatchEventKinds.ENTRY_DELETE));
		Assert.assertTrue(!watcher.isWatching(fileName, StandardWatchEventKinds.ENTRY_CREATE));
	}

	@Test
	public void shouldThrowExceptionRemovingNonExistentFile()
	{
		try {
			watcher.removeFile("nonExistentFile");
			exception.expect(IllegalArgumentException.class);
		} catch (IllegalArgumentException e) {}

		String fileName = testFile.getName();
		watcher.addFile(fileName);
		try {
			watcher.removeFile(fileName, StandardWatchEventKinds.ENTRY_DELETE);
			exception.expect(IllegalArgumentException.class);
		} catch (IllegalArgumentException e) {}
	}

	@Test
	public void shouldCorrectlyRemoveFileEventAssociation()
	{
		String fileName = testFile.getName();
		watcher.addFile(fileName, StandardWatchEventKinds.ENTRY_MODIFY, StandardWatchEventKinds.ENTRY_DELETE);

		ArrayList<WatchEvent.Kind<?>> events = watcher.getWatchedFiles().get(fileName);
		Assert.assertEquals(2, events.size());
		Assert.assertTrue(events.contains(StandardWatchEventKinds.ENTRY_MODIFY));
		Assert.assertTrue(events.contains(StandardWatchEventKinds.ENTRY_DELETE));

		watcher.removeFile(fileName);
		events = watcher.getWatchedFiles().get(fileName);
		Assert.assertEquals(1, events.size());
		Assert.assertTrue(events.contains(StandardWatchEventKinds.ENTRY_DELETE));
		Assert.assertTrue(!events.contains(StandardWatchEventKinds.ENTRY_MODIFY));

		watcher.removeFile(fileName, StandardWatchEventKinds.ENTRY_DELETE);
		Map<String, ArrayList<WatchEvent.Kind<?>>> files = watcher.getWatchedFiles();
		Assert.assertEquals(0, files.size());

		watcher.addFile(fileName, StandardWatchEventKinds.ENTRY_MODIFY, StandardWatchEventKinds.ENTRY_DELETE);
		watcher.removeFile(fileName, StandardWatchEventKinds.ENTRY_MODIFY, StandardWatchEventKinds.ENTRY_DELETE);
		files = watcher.getWatchedFiles();
		Assert.assertEquals(0, files.size());
	}

	@Test(expected=IllegalArgumentException.class)
	public void shouldThrowExceptionListeningToNonExistentFile()
	{
		@Ignore
		class TestListener implements WatcherListener
		{
			public void onModify(WatcherEvent event) {}
			public void onDelete(WatcherEvent event) {}
			public void onCreate(WatcherEvent event) {}
		}
		watcher.addListener("nonExistentFile", new TestListener());
	}

	@Test(expected=UnsupportedOperationException.class)
	public void shouldThrowExceptionIfNoFileWatched()
	{
		watcher.startWatching();
	}

	@Test(expected=UnsupportedOperationException.class)
	public void shouldThrowExceptionIfAlreadyWatching()
	{
		watcher.addFile(testFile.getName());
		watcher.startWatching();
		watcher.startWatching();
	}

	@Test
	public void shouldntGetNotificationIfNothingHappens()
	{
		String fileName = testFile.getName();
		watcher.addFile(fileName);

		@Ignore
		class TestListener implements WatcherListener
		{
			public boolean watchedModify = false;
			public boolean watchedCreate = false;
			public boolean watchedDelete = false;
			public void onModify(WatcherEvent event) { watchedModify = true; }
			public void onCreate(WatcherEvent event) { watchedCreate = true; }
			public void onDelete(WatcherEvent event) { watchedDelete = true; }
		}

		TestListener listener = new TestListener();
		watcher.addListener(fileName, listener);

		long startingTime = Calendar.getInstance().getTime().getTime();
		long elapsedTime = 0;

		watcher.startWatching();
		while(true) {
			elapsedTime = Calendar.getInstance().getTime().getTime() - startingTime;
			if (elapsedTime > 500) break;
		}

		Assert.assertFalse(listener.watchedModify);
		Assert.assertFalse(listener.watchedCreate);
		Assert.assertFalse(listener.watchedDelete);
	}

	@Test
	public void shouldDetectModifyEvent()
	{
		String fileName = testFile.getName();
		watcher.addFile(fileName);

		@Ignore
		class TestListener implements WatcherListener
		{
			public boolean watchedModify = false;
			public void onModify(WatcherEvent event) { watchedModify = true; }
			public void onCreate(WatcherEvent event) {}
			public void onDelete(WatcherEvent event) {}
		}

		TestListener listener = new TestListener();
		watcher.addListener(fileName, listener);

		long startingTime = Calendar.getInstance().getTime().getTime();
		long elapsedTime = 0;

		watcher.startWatching();
		while(true) {
			elapsedTime = Calendar.getInstance().getTime().getTime() - startingTime;
			if (elapsedTime > 500 || listener.watchedModify)
				break;
		}

		Assert.assertTrue(listener.watchedModify);
	}
}
