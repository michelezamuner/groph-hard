.PHONY: build jar run test tests install clean

build: src/app/*.java src/app/util/*.java
	@javac -Xlint:unchecked -d build/app src/app/*.java src/app/util/*.java

jar: build
	@cd build/app && jar -cmvf ../manifest.mf ../app.jar *.class

run: build
	@java -classpath build/app Main

tests: build
	@javac -Xlint:unchecked -cp lib/*:build/app -d build/tests src/tests/*.java
	@for className in $$(cat tests); do \
		if [ -f "build/tests/$$className.class" ]; then \
			echo "Testing $$className"; \
			java -cp lib/*:build/app:build/tests org.junit.runner.JUnitCore $$className; \
			if [ ! $$? ]; then break; fi \
		fi \
	done

install:
	@wget -O lib/hamcrest-core-1.3.jar http://search.maven.org/remotecontent?filepath=org/hamcrest/hamcrest-core/1.3/hamcrest-core-1.3.jar
	@wget -O lib/junit-4.12.jar http://search.maven.org/remotecontent?filepath=junit/junit/4.12/junit-4.12.jar
	@wget -O lib/mockito-all-1.9.5.jar http://central.maven.org/maven2/org/mockito/mockito-all/1.9.5/mockito-all-1.9.5.jar

clean:
	@find build -name "*.class" -exec rm {} \;
	@rm -f build/app.jar
